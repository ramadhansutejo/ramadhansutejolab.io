---
title: "UBD-Mobile Mahasiswa"
description: "Mobile base academic system for students"
date: '2018-08-30'
link: 'https://play.google.com/store/apps/details?id=id.ac.binadarma.ubdmobilemahasiswa'
screenshot: 'ubd-mobile-mahasiswa.png'
layout: 'portfolio'
featured: 'true'
---

This is an **Native Android** app to facilitate students in accessing the academic system. Some of the advantages from this app are, students will get notifications of the Sempro or Semhas schedule, validated grades, course schedules, etc. Also included with several features:

* View Study Plan Sheet
* View Study Result Sheet
* View Payments History
* Create Pay Code
* Study Plan Entries
* Activate Course Schedule Reminder (Alarm)

[![Google Play](/img/android-badge.svg "UBD-Mobile Mahasiswa")](https://play.google.com/store/apps/details?id=id.ac.binadarma.ubdmobilemahasiswa)