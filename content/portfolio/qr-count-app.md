---
title: "QR Count App"
description: "Quick and Real Time Count App for Election"
link: https://github.com/htr3n/dera
screenshot: qr-count-app.jpg
date: '2018-06-21'
layout: 'portfolio'
featured: 'true'
---

This is an **Native Android** app for poll candidate. Colaborate with [Dony Wahyu ISP](https://www.facebook.com/dnaextrim) & [M. Bunyamin](https://www.facebook.com/amin.grim).