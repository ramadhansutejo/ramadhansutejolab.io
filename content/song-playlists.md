---
title: "Song Playlists 🎧"
date: 2018-08-25T19:04:17+07:00
draft: false
---

I'm not musician. And I also can't play music. But, if you wanna know what the senses of my music are and what songs I play when coding. This is my playlist on the music streaming platform:

* [Apple Music](https://itunes.apple.com/id/playlist/fav/pl.u-11zBJ73c8Jg51vZ)
* [Spotify](https://open.spotify.com/user/21krrod2vzeanreblcxkbubti/playlist/5ECzcpQi1qTAYSoIMPJV6T?si=b8RiO4JlRcuw82x6mh2YEw)