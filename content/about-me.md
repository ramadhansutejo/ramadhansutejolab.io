---
title: "About Me 👀"
date: 2018-08-25T19:01:41+07:00
draft: false
---

Full time programmer at [Bina Darma University](https://www.binadarmaa.c.id). I'm a Native Android Programmer, Native iOS Programmer, Software Developer. Comfort on Back-end Programming and I've Little bit Networking knowledge. And I'm also enthusiastic about graphic design and UI/UX design.